Rad
====

This is a BASH script that helps create radio playlists. It was created for 
the SDF internet radio station anonradio.net

The script is designed to add songs to a playlist and to give you a running 
total of how many hours/minutes/seconds you are up to so far, so that you know
when to put in a station ident or PSA or when to end your show.